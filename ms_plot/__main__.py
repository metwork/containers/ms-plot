from ms_plot.app import app


def run_dev() -> None:
    app.run_server(debug=True)


if __name__ == "__main__":
    run_dev()
