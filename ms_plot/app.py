import os
from typing import Optional, Tuple, Dict, List
from pathlib import Path
import logging
from urllib.parse import urlparse, parse_qs
import requests
from requests.exceptions import ConnectionError
from werkzeug.exceptions import InternalServerError
from flask import Flask, request, Request, session, Response
from flask_cors import CORS
import plotly.graph_objects as go
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from matchms import Spectrum
from matchms.importing import load_from_mgf
from matchms.filtering import normalize_intensities
from matchms_plotly import plot_spectrum, plot_spectra

server = Flask(__name__)
server.config["SECRET_KEY"] = "the random string"

app = dash.Dash(__name__, server=server)
app.layout = html.Div(
    children=[
        # dcc.Location(id="url", refresh=False),
        html.Div(id="message", style={"display": False}),
        dcc.Loading(
            id="loading-1",
            type="default",
            children=dcc.Store(id="spectra-data", data=[]),
            style={"marginTop": 200},
        ),
        dcc.Graph(id="graph", style={"display": "none"}),
        dcc.Store(id="token", data=""),
    ]
)

cors = CORS(server, resources={r"/*": {"origins": "*"}})

TOKEN_HEADER = "TOKEN_HEADER"
SESSION_TOKEN_LABEL = "x-token"

SPECTRA_DATA_TYPE = List[Tuple[str, str]]

log_format = logging.Formatter("[%(asctime)s] [%(levelname)s] %(message)s")
app.logger.handlers[0].setFormatter(log_format)
app.logger.setLevel(logging.INFO)


@server.route("/dash/")
def index():  # type: ignore
    token = get_token(request)
    if token:
        app.logger.debug("index header has token")
        app.layout = html.Div(
            children=[
                dcc.Location(id="url", refresh=False),
                html.Div(id="message", style={"display": "block"}),
                dcc.Loading(
                    id="loading-1",
                    type="default",
                    children=dcc.Store(id="spectra-data", data=[]),
                    style={"marginTop": 200},
                ),
                dcc.Graph(
                    id="graph",
                    style={"display": "none"},
                ),
                dcc.Store(id="token", data=token),
            ]
        )
        if not server.config.get("has_callback"):
            register_callback(app)
    return app.index()


def get_temp_data_path() -> Path:
    return Path(str(os.getenv("MSPLOT_TEMP_DATA_PATH")))


DATA_ERROR_MESSAGE = "⚡️ ERROR: Unable to get data"


class DataError(Exception):
    description = DATA_ERROR_MESSAGE

    def __init__(self, data_id: str, description: Optional[str] = None):
        self.data_id = data_id
        if description:
            self.description = description
        self.log_error()

    def log_error(self) -> None:
        app.logger.error("%s for %s", self.description, self.data_id)


def get_token(request_: Request) -> Optional[str]:
    token_header = os.getenv(TOKEN_HEADER)
    if token_header:
        token = request_.headers.get(token_header)
        if token:
            return str(token)
    return None


@server.after_request
def after_request_func(response: Response) -> Response:
    try:
        health_check(request.path)
    except InternalServerError as exc:
        return Response(exc.description, 500)
    return response


def health_check(pathname: str) -> None:
    if pathname == "/health":
        app.logger.debug("receive health_check")
        temp_data_path = get_temp_data_path()
        if not temp_data_path.exists():
            app.logger.error("temp data path not '%s' not exists", temp_data_path)
            raise InternalServerError("temp data path not exists")
        app.logger.debug("health_check success, will return 200")
    return None


def fetch_data(spectrum_id: str) -> None:
    app.logger.info("Try to fetch_data for %s", spectrum_id)
    data_ambassador_host = os.getenv("DATA_AMBASSADOR_HOST")
    url = f"{data_ambassador_host}/spectrum/{spectrum_id}"
    headers = {}
    token = session.get(SESSION_TOKEN_LABEL)
    if token:
        headers = {os.getenv(TOKEN_HEADER): token}
    try:
        app.logger.info("Send POST to data ambassador for %s", url)
        response = requests.post(url, headers=headers)
        app.logger.debug(
            "data ambassador response with status %s", response.status_code
        )
        status_code = response.status_code
        if status_code == 200:
            app.logger.info("Data ambassador success for data_id : %s", spectrum_id)
            return None
        data_error = f"Data ambassador return status {status_code}"
    except ConnectionError as exc:
        data_error = f"ConnectionError for {url} : {exc}"
    raise DataError(spectrum_id, data_error)


def fetch_data_if_not_exists(spectrum_id: str) -> None:
    app.logger.info("Try to fetch_data_if_not_exists for %s", spectrum_id)
    spectrum_path = get_spectrum_path(spectrum_id)
    if not spectrum_path.exists():
        app.logger.info(
            "File %s not exists, fetch data_ambassador for %s ",
            spectrum_path,
            spectrum_id,
        )
        fetch_data(spectrum_id)
    else:
        app.logger.info("data already exists")
    if not spectrum_path.exists():
        raise DataError(spectrum_id)
    app.logger.debug("fetch_data_if_not_exists success")


def get_spectrum_path(spectrum_id: str) -> Path:
    return get_temp_data_path() / f"{spectrum_id}.mgf"


def register_callback(app_: dash.Dash) -> None:
    @app_.callback(  # type: ignore
        Output(component_id="graph", component_property="style"),
        Output(component_id="graph", component_property="figure"),
        Output(component_id="spectra-data", component_property="data"),
        Output(component_id="message", component_property="style"),
        Output(component_id="message", component_property="children"),
        Input("url", "href"),
        Input("token", "data"),
    )
    def display_graph(
        url: str, token: str
    ) -> Tuple[go.Figure, Dict[str, str], bool, Dict[str, str], str]:

        app_.logger.info("display_graph for href %s", url)

        try:
            session[SESSION_TOKEN_LABEL] = token

            parsed_url = urlparse(url)
            res = parse_qs(parsed_url.query)
            spectrum_up_id_ = res.get("spectrum_up_id")

            if not spectrum_up_id_:
                raise DataError("No spectrum_id")
            spectrum_up_id = spectrum_up_id_[0]
            fetch_data_if_not_exists(spectrum_up_id)
            spectrum_up_name = res.get("spectrum_up_name", [""])[0]
            spectra_data = [(spectrum_up_id, spectrum_up_name)]
            spectrum_down_id = res.get("spectrum_down_id", [""])[0]
            spectrum_down_name = res.get("spectrum_down_name", [""])[0]
            if spectrum_down_id:
                fetch_data_if_not_exists(spectrum_down_id)
                spectra_data.append((spectrum_down_id, spectrum_down_name))

            app.logger.info("get data success with %s", spectra_data)

            if not spectra_data:
                app.logger.debug("no spectra_data to display_graph")
                return (
                    {"display": "none"},
                    dash.no_update,
                    False,
                    {"display": "block"},
                    DATA_ERROR_MESSAGE,
                )

            return (
                {"display": "block"},
                get_plot(spectra_data),
                True,
                {"display": "none"},
                "",
            )
        except Exception as exc:
            app.logger.error("ERROR plot_graph %s", exc)
        return (
            {"display": "none"},
            dash.no_update,
            False,
            {"display": "block"},
            DATA_ERROR_MESSAGE,
        )

    server.config["has_callback"] = True


def get_plot(
    spectra_data: SPECTRA_DATA_TYPE,
) -> go.Figure:
    app.logger.info(
        "get_plot for %s ",
        spectra_data,
    )
    spectrum_up_id, spectrum_up_name = spectra_data[0]
    spectrum_up = get_spectrum(spectrum_up_id)
    if len(spectra_data) > 1:
        spectrum_down_id, spectrum_down_name = spectra_data[1]
        spectrum_down = get_spectrum(spectrum_down_id)
        spectra = set_spectra_names(
            (spectrum_up, spectrum_down), (spectrum_up_name, spectrum_down_name)
        )
        fig = plot_spectra(*spectra)
    else:
        fig = plot_spectrum(spectrum_up)
    fig.update_layout(
        height=300,
        margin={"l": 20, "r": 20, "t": 20, "b": 0},
        legend={"yanchor": "top", "y": 0.99, "xanchor": "left", "x": 0.01},
    )
    return fig


def set_spectra_names(
    spectra: Tuple[Spectrum, Spectrum], names: Tuple[Optional[str], Optional[str]]
) -> Tuple[Spectrum, Spectrum]:
    new_spectra: List[Spectrum] = []
    for name, spectrum in zip(names, spectra):
        if name:
            metadata = spectrum.metadata
            metadata.update({"title": name})
            spectrum.metadata = metadata
        new_spectra.append(spectrum)
    return new_spectra[0], new_spectra[1]


def get_spectrum(spectrum_id: str) -> Spectrum:
    app.logger.info("get_spectrum for %s ", spectrum_id)
    spectrum_path = get_spectrum_path(spectrum_id)
    spectrum = list(load_from_mgf(str(spectrum_path)))[0]
    return normalize_intensities(spectrum)
