import subprocess
import shlex


def run() -> None:
    print("### Run Flake8 ###")
    cmd = "flake8"
    subprocess.run(shlex.split(cmd))

    print("### Run MyPy ###")
    cmd = "mypy --strict ."
    subprocess.run(shlex.split(cmd))

    print("### Run pytest ###")
    cmd = (
        "pytest --cov-report term-missing --cov=ms_plot "
        "--headless --disable-warnings"
    )
    subprocess.run(shlex.split(cmd))
