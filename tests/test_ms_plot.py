import time
from pathlib import Path
import shutil
from typing import List, Any, Callable
from contextlib import contextmanager
import pytest
from pytest import MonkeyPatch
import requests
from requests.exceptions import ConnectionError
import requests_mock
from dash.testing.composite import DashComposite
import ms_plot
from ms_plot.app import app, TOKEN_HEADER, DATA_ERROR_MESSAGE


DATA_AMBASSADOR_HOST = "http://data_ambassador:8888"


@pytest.fixture(autouse=True)
def set_envs(monkeypatch: MonkeyPatch) -> None:
    monkeypatch.setenv("DATA_AMBASSADOR_HOST", DATA_AMBASSADOR_HOST)
    monkeypatch.setenv(TOKEN_HEADER, "X-Token")


def test_version() -> None:
    toml_path = Path(__file__).parents[1] / "pyproject.toml"
    version_line = toml_path.read_text().split("\n")[2]
    version = version_line.split(" = ")[1]
    assert version == f'"{ms_plot.__version__}"'


def assert_data_error(dash_app: DashComposite) -> None:
    dash_app.wait_for_element("#message", timeout=4)
    assert dash_app.wait_for_contains_text("#message", DATA_ERROR_MESSAGE, timeout=4)

    dash_app.wait_for_element("#graph", timeout=0)
    assert not dash_app.find_element("#graph").is_displayed()


@pytest.fixture
def dash_app(
    dash_duo: DashComposite, monkeypatch: MonkeyPatch, shared_datadir: Path
) -> DashComposite:
    monkeypatch.setenv("MSPLOT_TEMP_DATA_PATH", str(shared_datadir))
    monkeypatch.setattr(ms_plot.app, "get_token", lambda x: "token_value")

    dash_duo.start_server(app)
    # assert_data_error(dash_duo)
    # dash_duo.wait_for_element("#graph", timeout=0)
    # assert not dash_duo.find_element("#graph").is_displayed()
    return dash_duo


def test_health_500(dash_duo: DashComposite) -> None:
    dash_duo.start_server(app)
    response = requests.get(f"{dash_duo.server_url}/health")
    assert response.status_code == 500


def test_health_200_data_path(
    dash_duo: DashComposite, monkeypatch: MonkeyPatch, shared_datadir: Path
) -> None:
    monkeypatch.setenv("MSPLOT_TEMP_DATA_PATH", str(shared_datadir))
    dash_duo.start_server(app)
    response = requests.get(f"{dash_duo.server_url}/health")
    assert response.status_code == 200


@pytest.fixture
def mock_data(
    shared_datadir: Path,
) -> Callable[[List[str]], Any]:
    @contextmanager
    def function(spectrum_ids: List[str]) -> Any:
        with requests_mock.mock() as mocker:
            for spectrum_id in spectrum_ids:

                def copy_data(spectrum_id: str) -> Callable[[List[Any]], Any]:
                    def function(*args, **kwargs) -> None:  # type: ignore
                        source_path = shared_datadir / "to_copy" / f"{spectrum_id}.mgf"
                        if source_path.exists():
                            shutil.copy(
                                source_path,
                                shared_datadir / f"{spectrum_id}.mgf",
                            )

                    return function

                mocker.post(
                    f"{DATA_AMBASSADOR_HOST}/spectrum/{spectrum_id}",
                    text=copy_data(spectrum_id),
                )
            yield mocker

    return function


@pytest.fixture
def app_url(dash_app: DashComposite) -> str:
    return f"{dash_app.server_url}/dash"


def test_dash_spectrum(
    dash_app: DashComposite, app_url: str, mock_data: Callable[[List[str]], Any]
) -> None:

    with mock_data(["test-1"]) as mocker:
        dash_app.wait_for_page(f"{app_url}/?spectrum_up_id=test-1", timeout=4)
        dash_app.wait_for_element("#graph", timeout=4)
        time.sleep(1)
        assert dash_app.find_element("#graph").is_displayed()
        assert not dash_app.find_element("#message").is_displayed()
        assert len(mocker.request_history) == 1
        assert dash_app.get_logs() == [], "browser console should contain no error"


def test_dash_spectra(
    dash_app: DashComposite, app_url: str, mock_data: Callable[[List[str]], Any]
) -> None:

    with mock_data(["test-1", "test-2"]) as mocker:
        dash_app.wait_for_page(
            f"{app_url}/?spectrum_up_id=test-1&spectrum_down_id=test-2",
            timeout=4,
        )
        dash_app.wait_for_element("#graph", timeout=4)
        time.sleep(1)

        assert dash_app.find_element("#graph").is_displayed()
        assert not dash_app.find_element("#message").is_displayed()
        print(mocker.request_history)
        assert len(mocker.request_history) == 2
        assert dash_app.get_logs() == [], "browser console should contain no error"


def test_dash_spectra_names(
    dash_app: DashComposite, app_url: str, mock_data: Callable[[List[str]], Any]
) -> None:

    input_names = ["name_1", "name_2"]

    with mock_data(["test-1", "test-2"]):
        dash_app.wait_for_page(
            f"{app_url}/?spectrum_up_id=test-1&spectrum_down_id=test-2"
            f"&spectrum_up_name={input_names[0]}&spectrum_down_name={input_names[1]}",
            timeout=4,
        )
        dash_app.wait_for_element("#graph", timeout=4)
        time.sleep(1)

        assert dash_app.find_element("#graph").is_displayed()
        assert not dash_app.find_element("#message").is_displayed()
        output_names = [el.text for el in dash_app.find_elements(".legendtext")]
        assert input_names == output_names


def test_no_data_folder(
    dash_duo: DashComposite,
    monkeypatch: MonkeyPatch,
    mock_data: Callable[[List[str]], Any],
) -> None:

    monkeypatch.setenv("MSPLOT_TEMP_DATA_PATH", "/not_exists")
    monkeypatch.setattr(ms_plot.app, "get_token", lambda x: "token_value")
    dash_duo.start_server(app)

    with mock_data(["test-1"]):
        dash_duo.wait_for_page(
            f"{dash_duo.server_url}/dash/?spectrum_up_id=test-1", timeout=4
        )
        assert_data_error(dash_duo)


def test_no_data_file(
    dash_app: DashComposite, mock_data: Callable[[List[str]], Any]
) -> None:

    with mock_data(["test-1", "test-3"]):
        dash_app.wait_for_page(
            f"{dash_app.server_url}/dash/?spectrum_up_id=test-3", timeout=4
        )
        assert_data_error(dash_app)
        dash_app.wait_for_page(
            (
                f"{dash_app.server_url}/dash/"
                "?spectrum_up_id=test-1&spectrum_down_id=test-3"
            ),
            timeout=4,
        )
        assert_data_error(dash_app)


def test_no_data_connection(dash_app: DashComposite, app_url: str) -> None:

    with requests_mock.mock() as mocker:
        mocker.post(f"{DATA_AMBASSADOR_HOST}/spectrum/test-1", exc=ConnectionError)
        dash_app.wait_for_page(f"{app_url}/?spectrum_up_id=test-1", timeout=4)
        assert_data_error(dash_app)


def test_token(
    dash_duo: DashComposite,
    monkeypatch: MonkeyPatch,
    shared_datadir: Path,
    mock_data: Callable[[List[str]], Any],
) -> None:

    monkeypatch.setenv("MSPLOT_TEMP_DATA_PATH", str(shared_datadir))
    monkeypatch.setenv("TOKEN_HEADER", "X-Token")
    monkeypatch.setattr(ms_plot.app, "get_token", lambda x: "token_value")
    dash_duo.start_server(app)

    with mock_data(["test-1"]):
        dash_duo.wait_for_page(
            f"{dash_duo.server_url}/dash/?spectrum_up_id=test-1", timeout=4
        )
        dash_duo.wait_for_element("#graph", timeout=4)
        time.sleep(1)
        assert dash_duo.find_element("#graph").is_displayed()
